import copy
from pyunicorn import climate, funcnet, timeseries, core
from pyunicorn.core.geo_grid import GeoGrid
from plot import plot_measure, plot_network
import scipy.stats.mstats as mstats
import os
import shutil
import numpy as np
from pyunicorn import core
import matplotlib.pyplot as plt
from plot import plot_map, plot_cross_map
import pickle

#
#/p/projects/synchronet/Users/jamir/climate_data/for_use/grid05/2003_2019/
OBSERVABLE1=['GPP',"/home/eggo/piktmp/climate_data/for_use/grid05/2003_2019/GPP.nc"]
OBSERVABLE2=['E',"/home/eggo/piktmp/climate_data/for_use/grid05/2003_2019/E.nc"]
OBSERVABLE3=['ssrd',"/home/eggo/piktmp/climate_data/for_use/grid05/2003_2019/ssrd.nc"]
OBSERVABLE4=['precipitation',"/home/eggo/piktmp/climate_data/for_use/grid05/2003_2019/precipitation.nc"]
OBSERVABLE5=['t2m',"/home/eggo/piktmp/climate_data/for_use/grid05/2003_2019/t2m.nc"]


obs_combinations=[[OBSERVABLE1,OBSERVABLE2],[OBSERVABLE1,OBSERVABLE3],[OBSERVABLE1,OBSERVABLE4],[OBSERVABLE1,OBSERVABLE5],[OBSERVABLE2,OBSERVABLE3],[OBSERVABLE2,OBSERVABLE4],[OBSERVABLE2,OBSERVABLE5],[OBSERVABLE3,OBSERVABLE4],[OBSERVABLE3,OBSERVABLE5],[OBSERVABLE4,OBSERVABLE5]]
select_combinations=obs_combinations[2:3]
num_combinations = len(select_combinations)





# temporal and spatial window of interest
WINDOW1 = {"time_min": 0., "time_max": 0., "lat_min": -1
    , "lon_min": -55
    , "lat_max": 0
    , "lon_max": -54}
WINDOW2 ={"time_min": 0., "time_max": 0., "lat_min": -1
    , "lon_min": -55
    , "lat_max": 0
    , "lon_max": -54}
time_range = '2003_2019'


job_nr = 4  # number of parallel executions, must be factor of M and compare_nr
nr_surrogates = 500  # number of surrogate time series for each cell
M = 24  # number of surrogate networks (sern)
perc = 10 # percentile that serves as threshold for correlation between timeseries
spatial_perc = 99  # percentile of the M serns
tau = 3  # number of lags
nsi = 0
compare_nr = 8  # must be factor of M
plots = True

noise = 0  # if true noise is added to deseasonalized data
noise_factor = 0.1  # factor to scale mean of deseasonalized data. scaled mean is the noise added

save_sern_adj=0 # if 1, sern adjacency matrices are savedsave_sern_adj=0 # if 1, sern adjacency matrices are saved

compare_factor = int(M / compare_nr)
if M / job_nr - int(M / job_nr) != 0:
    raise ValueError("job nr must be factor of M")

if compare_factor - M / compare_nr != 0:
    raise ValueError("job nr must be factor of M")

#  Path and filename of NetCDF file containing climate data

print("time range= {}".format(time_range))

EARTH_RADIUS = 6367.5

#  Type of data file ("NetCDF" indicates a NetCDF file with data on a regular
#  lat-lon grid, "iNetCDF" allows for arbitrary grids - > see documentation).
FILE_TYPE = "NetCDF"
#  Indicate the length of the annual cycle in the data (e.g., 12 for monthly
#  data)
TIME_CYCLE = 12

#  Indicates whether to use only data from winter months (DJF) for calculating
#  correlations
WINTER_ONLY = False



def cos_lat(lat_sequence):
    return np.cos(lat_sequence * np.pi / 180)

for p in range(num_combinations):
    OBSERVABLE_NAME1=select_combinations[p][0][0]
    OBSERVABLE_NAME2 = select_combinations[p][1][0]
    print(OBSERVABLE_NAME2)
    DATA_FILENAME1 = select_combinations[p][0][1]
    DATA_FILENAME2 = select_combinations[p][1][1]



    #
    # create filename
    #

    filename = OBSERVABLE_NAME1 + '_' + OBSERVABLE_NAME2 + '_' + 'lat_min={}'.format(
        WINDOW1['lat_min']) + '_{}'.format(WINDOW2['lat_min']) + '_' + 'lon_min={}'.format(
        WINDOW1['lon_min']) + '_{}'.format(WINDOW2['lon_min']) + '_' \
               + 'lat_max={}'.format(WINDOW1['lat_max']) + '_{}'.format(WINDOW2['lat_max']) + '_' + 'lon_max={}'.format(
        WINDOW1['lon_max']) \
               + '_{}'.format(WINDOW2['lon_max']) + '_perc={}'.format(perc) + '_tau_max={}'.format(
        tau) + '_nsi={}'.format(
        nsi) + '_' + time_range

    #
    # create results folder
    #

    current_directory = os.getcwd()
    final_directory = os.path.join(current_directory, filename)
    if not os.path.exists(final_directory):
        os.makedirs(final_directory)
    with open('final_directory.txt', "w") as text_file:
        text_file.write(final_directory)
    with open('final_directory.pkl', 'wb') as f:
        pickle.dump(final_directory, f)
    with open(final_directory + '/{}_WINDOW.pkl'.format(OBSERVABLE_NAME1), 'wb') as f:
        pickle.dump(WINDOW1, f)
    with open(final_directory + '/{}_WINDOW.pkl'.format(OBSERVABLE_NAME2), 'wb') as f:
        pickle.dump(WINDOW2, f)

    with open(final_directory + '/OBSERVABLE_NAME1.pkl', 'wb') as f:
        pickle.dump(OBSERVABLE_NAME1, f)
    with open(final_directory + '/OBSERVABLE_NAME2.pkl', 'wb') as f:
        pickle.dump(OBSERVABLE_NAME2, f)

    with open(final_directory + '/OBSERVABLE_NAME1.txt', "w") as text_file:
        text_file.write(OBSERVABLE_NAME1)
    with open(final_directory + '/OBSERVABLE_NAME2.txt', "w") as text_file:
        text_file.write(OBSERVABLE_NAME2)

    with open(final_directory + '/job_nr.pkl', 'wb') as f:
        pickle.dump(job_nr, f)
    with open(final_directory + '/nr_surrogates.pkl', 'wb') as f:
        pickle.dump(nr_surrogates, f)
    with open(final_directory + '/tau.pkl', 'wb') as f:
        pickle.dump(tau, f)

    with open(final_directory + '/M.pkl', 'wb') as f:
        pickle.dump(M, f)
    with open(final_directory + '/perc.pkl', 'wb') as f:
        pickle.dump(perc, f)
    with open(final_directory + '/spatial_perc.pkl', 'wb') as f:
        pickle.dump(spatial_perc, f)
    with open(final_directory + '/nsi.pkl', 'wb') as f:
        pickle.dump(nsi, f)
    with open(final_directory + '/compare_nr.pkl', 'wb') as f:
        pickle.dump(compare_nr, f)
    with open(final_directory + '/save_sern_adj.pkl', 'wb') as f:
        pickle.dump(save_sern_adj, f)
    with open(final_directory + '/time_range.pkl', 'wb') as f:
        pickle.dump(time_range, f)

    #
    # copy main program to results folder, so results can be reproduced
    #
    shutil.copy(current_directory + '/all_without_sern_threshold_c.py', final_directory)

    #
    #  Create a ClimateData object containing the data and print information
    #
    # initiate Climate Data Class with land data
    DATA1 = climate.ClimateData.Load(
        file_name=DATA_FILENAME1, observable_name=OBSERVABLE_NAME1, file_type=FILE_TYPE,
        window=WINDOW1, time_cycle=TIME_CYCLE)

    # get mask from land data
    mask = np.ma.getmask(DATA1.observable())
    # get deseasonalized data and set masked values to nan
    anomaly1 = np.where(mask == True, np.nan, DATA1.anomaly())
    # get land nodes
    land_nodes = np.ndarray.flatten(np.argwhere(~np.isnan(anomaly1[0, :])))
    np.savetxt(final_directory + '/{}'.format(OBSERVABLE_NAME1)+'_nodes.txt', land_nodes, fmt='%1.1i')

    # get ocean nodes
    ocean_nodes1 = np.ndarray.flatten(np.argwhere(np.isnan(anomaly1[0, :])))
    anomaly1_reduced = np.delete(anomaly1, ocean_nodes1, 1)

    # initiate Climate Data Class with atmosphere data
    DATA2 = climate.ClimateData.Load(
        file_name=DATA_FILENAME2, observable_name=OBSERVABLE_NAME2, file_type=FILE_TYPE,
        window=WINDOW2, time_cycle=TIME_CYCLE)

    # get deseasonalized atmos data
    anomaly2 = DATA2.anomaly()
    # get mask from land data
    mask = np.ma.getmask(DATA2.observable())
    # get deseasonalized data and set masked values to nan
    anomaly2 = np.where(mask == True, np.nan, DATA2.anomaly())
    atmos_nodes = np.ndarray.flatten(np.argwhere(~np.isnan(anomaly2[0, :])))
    np.savetxt(final_directory + '/{}'.format(OBSERVABLE_NAME2)+'_nodes.txt', atmos_nodes, fmt='%1.1i')
    ocean_nodes = np.ndarray.flatten(np.argwhere(np.isnan(anomaly2[0, :])))
    anomaly2_reduced = np.delete(anomaly2, ocean_nodes, 1)
    joined_anomaly = np.concatenate((anomaly1[0, :], anomaly2[0, :]), axis=0)

    nodes_without_ocean = np.ndarray.flatten(np.argwhere(~np.isnan(joined_anomaly)))
    np.savetxt(final_directory + '/nodes_without_ocean.txt', nodes_without_ocean, fmt='%1.1i')

    #
    # add noise to deseasonalized data
    #



    # rank anomaly values for rank correlation
    anomaly1_ranked = mstats.rankdata(anomaly1_reduced, axis=0)  # anomaly1_reduced.argsort(axis=0).argsort(axis=0) + 1.0#
    anomaly2_ranked = mstats.rankdata(anomaly2_reduced, axis=0)  # anomaly2.argsort(axis=0).argsort(axis=0) + 1.0#
    # anomaly = np.concatenate((anomaly1_reduced, anomaly2), axis=1) # pearson
    anomaly = np.concatenate((anomaly1_ranked, anomaly2_ranked),
                             axis=1)  # take ranked values for spearman rank correlation

    #
    #
    # get grid information
    #

    lat_sequence1 = DATA1.grid.lat_sequence()
    land_lat_sequence = lat_sequence1[land_nodes]

    lon_sequence1 = DATA1.grid.lon_sequence()
    land_lon_sequence = lon_sequence1[land_nodes]

    lat_sequence2 = DATA2.grid.lat_sequence()
    atmos_lat_sequence = lat_sequence2[atmos_nodes]

    lon_sequence2 = DATA2.grid.lon_sequence()
    atmos_lon_sequence = lon_sequence2[atmos_nodes]

    lats_joined = np.concatenate((lat_sequence1, lat_sequence2))
    reduced_lats_joined = np.concatenate((land_lat_sequence, atmos_lat_sequence))
    lons_joined = np.concatenate((lon_sequence1, lon_sequence2))

    reduced_lons_joined = np.concatenate((land_lon_sequence, atmos_lon_sequence))
    lons = [lon_sequence1, lon_sequence2, lons_joined]
    lats = [lat_sequence1, lat_sequence2, lats_joined]
    # get number of nodes in network 1
    n_1 = len(lons[0])
    # get number of nodes in network 2
    n_2 = len(lons[1])
    N = len(lons[2])
    # get total number of nodes in joined network
    Nodes = range(N)

    nr_land_nodes = len(land_nodes)
    nr_nodes_without_ocean = len(nodes_without_ocean)
    nr_atmos_nodes = len(atmos_nodes)
    land_node_list = range(nr_land_nodes)
    atmos_node_list = range(nr_land_nodes, nr_nodes_without_ocean, 1)
    nodelist_without_ocean=range(nr_nodes_without_ocean)


    lat1 = np.unique(lat_sequence1, return_counts=True)
    lat2 = np.unique(lat_sequence2, return_counts=True)
    lon1 = np.unique(lon_sequence1, return_counts=True)
    lon2 = np.unique(lon_sequence2, return_counts=True)
    lat_joined = np.unique(lats_joined)
    lon_joined = np.unique(lons_joined)

    reduced_lat_joined = np.unique(reduced_lats_joined)
    reduced_lon_joined = np.unique(reduced_lons_joined)
    nr_lat = [len(lat1[0]), len(lat2[0]), len(lat_joined)]
    nr_lon = [len(lon1[0]), len(lon2[0]), len(lon_joined)]
    lon = [lon1[0], lon2[0], lon_joined]
    lat = [lat1[0], lat2[0], lat_joined]


    nr_time = anomaly1_ranked.shape[0]

    #
    # save grid info
    #

    gridname1 = final_directory + "/" + 'grid1.obj'
    gridname2 = final_directory + "/" + 'grid2.obj'

    DATA1.grid.save(gridname1)
    DATA2.grid.save(gridname2)
    np.savetxt(final_directory + "/" + '{}_nodes.txt'.format(OBSERVABLE_NAME1), land_nodes, fmt='%1.1i')
    np.savetxt(final_directory + "/" + '{}_nodes.txt'.format(OBSERVABLE_NAME2), land_nodes, fmt='%1.1i')
    np.savetxt(final_directory + "/" + 'nodes_without_ocean.txt', nodes_without_ocean, fmt='%1.1i')
    np.savetxt(final_directory + "/" + 'lat1.txt', lat_sequence1)
    np.savetxt(final_directory + "/" + 'lon1.txt', lon_sequence1)
    np.savetxt(final_directory + "/" + 'lat2.txt', lat_sequence2)
    np.savetxt(final_directory + "/" + 'lon2.txt', lon_sequence2)

    #
    # construct coupling analysis instance for all data, land data and atmos data
    #
    print('calculating correlations ...')
    FUNCNET = funcnet.CouplingAnalysis(anomaly)

    #
    # get max correlation and corresponding lag
    #
    corr, lag = FUNCNET.cross_correlation(tau_max=tau, lag_mode='max')


    #
    # get lags
    #
    LAGS = [lag[0:nr_land_nodes, nr_land_nodes:nr_nodes_without_ocean],np.transpose(lag[nr_land_nodes:nr_nodes_without_ocean, 0:nr_land_nodes])]

    land_LAGS = [lag[0:nr_land_nodes, 0:nr_land_nodes],np.transpose(lag[0:nr_land_nodes, 0:nr_land_nodes])]
    atmos_LAGS = [lag[nr_land_nodes:nr_nodes_without_ocean, nr_land_nodes:nr_nodes_without_ocean],np.transpose(lag[nr_land_nodes:nr_nodes_without_ocean, nr_land_nodes:nr_nodes_without_ocean])]

    # np.savetxt(final_directory + '/atmos_lags_matrix.txt', atmos_LAGS[0])
    # np.savetxt(final_directory + '/land_lags_matrix.txt', land_LAGS[0])

    #
    # get surrogates
    #

    # initiate Surrogates instance with land data
    surrogates1 = np.empty((anomaly1_ranked.shape[0], anomaly1_ranked.shape[1], nr_surrogates),dtype='float16')

    rand_correlation_max_land_1 = np.empty((nr_land_nodes, nr_surrogates),dtype='float16')

    rand_correlation_max_land_2 = copy.deepcopy(rand_correlation_max_land_1)
    # get correlated noise surrogates for land data
    for i in range(nr_surrogates):
        surrogate = timeseries.surrogates.Surrogates(np.transpose(anomaly1_ranked), silence_level=2)
        surrogates1[:, :, i] = np.transpose(surrogate.correlated_noise_surrogates(np.transpose(anomaly1_ranked)))
    # calculate correlation to surrogates
    for i in range(nr_land_nodes):
        funcnet1 = funcnet.CouplingAnalysis(anomaly1_ranked[:, i], silence_level=2)
        rand_correlation_max_land_1[i] = funcnet1.pairwise_cross_correlation(surrogates1[:, i, :], tau_max=tau)[0]
        rand_correlation_max_land_2[i] = np.transpose(
            funcnet1.pairwise_cross_correlation_reverse(surrogates1[:, i, :], tau_max=tau)[0])

    surrogates2 = np.empty((anomaly2_ranked.shape[0], anomaly2_ranked.shape[1], nr_surrogates),dtype='float16')

    rand_correlation_max_atmos_1 = np.empty((n_2, nr_surrogates),dtype='float16')
    rand_correlation_max_atmos_2 = copy.deepcopy(rand_correlation_max_atmos_1)
    # get correlated noise surrogates for land data
    for i in range(nr_surrogates):
        surrogate = timeseries.surrogates.Surrogates(np.transpose(anomaly2_ranked), silence_level=2)
        surrogates2[:, :, i] = np.transpose(surrogate.correlated_noise_surrogates(np.transpose(anomaly2_ranked)))

    for i in range(nr_atmos_nodes):
        funcnet2 = funcnet.CouplingAnalysis(anomaly2_ranked[:, i], silence_level=2)
        rand_correlation_max_atmos_1[i] = funcnet2.pairwise_cross_correlation(surrogates2[:, i, :], tau_max=tau)[0]
        rand_correlation_max_atmos_2[i] = np.transpose(
            funcnet2.pairwise_cross_correlation_reverse(surrogates2[:, i, :], tau_max=tau)[0])
    anomaly = None

    # concatenate both into one matrix
    rand_correlation_max_1 = np.concatenate((rand_correlation_max_land_1, rand_correlation_max_atmos_1), axis=0,dtype='float16')
    rand_correlation_max_land_1 = None
    rand_correlation_max_atmos_1 = None
    rand_correlation_max_2 = np.concatenate((rand_correlation_max_land_2, rand_correlation_max_atmos_2), axis=0,dtype='float16')
    rand_correlation_max_land_2 = None
    rand_correlation_max_atmos_2 = None

    # get positive correlations
    rand_correlation_max_1_pos = np.empty(rand_correlation_max_1.shape[0])
    rand_correlation_max_1_pos = np.where(rand_correlation_max_1 > 0, rand_correlation_max_1, np.nan)

    rand_correlation_max_2_pos = np.empty(rand_correlation_max_2.shape[0])
    rand_correlation_max_2_pos = np.where(rand_correlation_max_2 > 0, rand_correlation_max_2, np.nan)

    # get perc-percentile of the positive correlations for each node
    rand_corr_pos_1 = np.nanpercentile(rand_correlation_max_1_pos, perc, axis=1)
    rand_correlation_max_1_pos = None
    rand_corr_pos_2 = np.nanpercentile(rand_correlation_max_2_pos, perc, axis=1)
    rand_correlation_max_2_pos = None



    # get perc-percentile of the negative correlations for each node
    rand_correlation_max_1_neg = np.empty(rand_correlation_max_1.shape[0])
    rand_correlation_max_1_neg = np.where(rand_correlation_max_1 < 0, -rand_correlation_max_1, np.nan)
    rand_correlation_max_2_neg = np.empty(rand_correlation_max_2.shape[0])
    rand_correlation_max_2_neg = np.where(rand_correlation_max_2 < 0, -rand_correlation_max_2, np.nan)

    rand_corr_neg_1 = np.nanpercentile(rand_correlation_max_1_neg, perc, axis=1)
    rand_correlation_max_1_neg = None
    rand_corr_neg_2 = np.nanpercentile(rand_correlation_max_2_neg, perc, axis=1)
    rand_correlation_max_2_neg = None

    # list with positive and negative correlation percentiles for both directions
    rand_corr_1 = [rand_corr_pos_1, rand_corr_neg_1]
    rand_corr_2 = [rand_corr_pos_2, rand_corr_neg_2]
    rand_corr = [rand_corr_1, rand_corr_2]

    signum = ['positive', 'negative']
    sign = [1, -1]
    s = 0
    OBSERVABLE_NAME_1 = [OBSERVABLE_NAME1, OBSERVABLE_NAME2]
    OBSERVABLE_NAME_2 = [OBSERVABLE_NAME2, OBSERVABLE_NAME1]
    #
    # start loop for positive and then negative correlations
    #
    for s in [0, 1]:
        print(signum[s])

        #
        # create folders for the results
        #
        signum_directory = os.path.join(final_directory, signum[s])
        if not os.path.exists(signum_directory):
            os.makedirs(signum_directory)

        # create threshold matrix
        corr_sign = np.where(sign[s] * corr > 0.0000, sign[s] * corr, 0)
        print(corr_sign)



        #
        # loop over both directions, land -> atmos and atmos -> land
        #

        for x in range(2):

            #
            # create subfolders for results
            #
            dir_directory = os.path.join(signum_directory, OBSERVABLE_NAME_1[x] + '-' + OBSERVABLE_NAME_2[x])
            if not os.path.exists(dir_directory):
                os.makedirs(dir_directory)
            # np.savetxt(dir_directory + '/cross_lags_matrix.txt', LAGS[s])
            land_directory = os.path.join(dir_directory, OBSERVABLE_NAME1)
            if not os.path.exists(land_directory):
                os.makedirs(land_directory)
            atmos_directory = os.path.join(dir_directory, OBSERVABLE_NAME2)
            if not os.path.exists(atmos_directory):
                os.makedirs(atmos_directory)

            #
            # save variables in filename
            #
            info_name = OBSERVABLE_NAME_1[x] + '-' + OBSERVABLE_NAME_2[x] + '_percentile={}'.format(perc) \
                        + '_' + signum[s]
            rand_corr_sign = rand_corr[x][s]
            threshold = np.empty(corr_sign.shape, dtype='float32')
            for ii in range(nr_land_nodes):
                threshold[ii, 0:ii + 1] = rand_corr_sign[ii]
            for jj in range(nr_land_nodes, nr_nodes_without_ocean, 1):
                threshold[jj, 0:nr_land_nodes] = rand_corr_sign[jj]
                threshold[jj, nr_land_nodes:jj] = rand_corr_sign[jj]

            u_indices = np.triu_indices_from(threshold, k=1)
            threshold[u_indices] = np.transpose(threshold)[u_indices]
            # take the perc-percentile as threshold
            threshold_land = np.empty(n_1)
            threshold_land.fill(np.nan)

            threshold_land[land_nodes] = rand_corr_sign[0:nr_land_nodes]

            np.savetxt(dir_directory + '/threshold_' + OBSERVABLE_NAME1 + '_' + signum[s] + '.txt', threshold_land)
            threshold_atmos = np.empty(n_2)
            threshold_atmos.fill(np.nan)
            threshold_atmos[atmos_nodes] = rand_corr_sign[nr_land_nodes:nr_nodes_without_ocean]

            np.savetxt(dir_directory + '/threshold_' + OBSERVABLE_NAME2 + '_' + signum[s] + '.txt',
                       threshold_atmos)
            # save and plot threshold for each node
            label = 'threshold, percentile: {}'.format(perc)
            plt.close()
            plot_measure(threshold_land, lon[0], lat[0], nr_lon[0], nr_lat[0], label)
            plt.savefig(dir_directory + '/threshold_' + OBSERVABLE_NAME1 + '_' + signum[s] + '.pdf')
            plt.close()

            plot_measure(threshold_atmos, lon[1], lat[1], nr_lon[1], nr_lat[1], label)
            plt.savefig(dir_directory + '/threshold_' + OBSERVABLE_NAME2 + '_' + signum[s] + '.pdf')
            plt.close()


            if x == 1:
                # separating directions of potential causality by getting corresponding triangles of max correlation matrix
                correlation = np.tril(corr_sign,
                                      0)  # get lower triangle for Observable 2 -> Observable 1 (negative tau)

            else:
                correlation = np.transpose(
                    np.triu(corr_sign, 0))  # get upper triangle for Observable 1 -> Observable 2 (positive tau)


            # construct symmetric matrix
            u_indices = np.triu_indices_from(correlation)
            correlation[u_indices] = np.transpose(correlation)[u_indices]

            # thresholding
            correlation = np.where(correlation > threshold, 1, 0).astype('uint8')
            np.fill_diagonal(correlation, 0)
            threshold = None

            #
            # angular great circle distance
            #
            grid = GeoGrid(np.arange(DATA1.grid.grid_size()["time"]),
                           np.concatenate((land_lat_sequence, lat_sequence2)),
                           np.concatenate((land_lon_sequence, lon_sequence2)))


            CORR = correlation[nr_land_nodes:nr_nodes_without_ocean, 0:nr_land_nodes]

            #
            # construct coupled climate network
            #
            net = core.interacting_networks.InteractingNetworks(correlation)
            # sanity check
            if np.allclose(correlation, net.internal_adjacency(nodelist_without_ocean)) == False:
                raise ValueError("problem")
            correlation = None
            #
            # get adjacency matrices for correlation between land and atmos, and for each of them alone
            # and save them
            adj_name = dir_directory + "/" + 'adjacency_' + info_name + '.txt'
            land_adj_name = land_directory + "/" + 'adjacency_' + info_name + '.txt'
            atmos_adj_name = atmos_directory + "/" + 'adjacency_' + info_name + '.txt'

            adj = net.cross_adjacency_sparse(land_node_list, atmos_node_list)
            np.savetxt(adj_name, adj)
            land_adj = net.cross_adjacency_sparse(land_node_list, land_node_list)
            np.savetxt(land_adj_name, land_adj)
            atmos_adj = net.cross_adjacency_sparse(atmos_node_list, atmos_node_list)
            np.savetxt(atmos_adj_name, atmos_adj)

            cross_density = np.sum(adj) / np.size(adj)
            land_density = np.sum(land_adj) / np.size(land_adj)
            atmos_density = np.sum(atmos_adj) / np.size(atmos_adj)

            with open(dir_directory + "/link_density.txt", "w") as file:
                file.write(str(cross_density))
            with open(land_directory + "/link_density.txt", "w") as file:
                file.write(str(land_density))
            with open(atmos_directory + "/link_density.txt", "w") as file:
                file.write(str(atmos_density))

            lags = np.extract(adj == 1, LAGS[x])  # get lags for cross links
            land_lags = np.extract(land_adj == 1, land_LAGS[x])  # get lags for land links
            atmos_lags = np.extract(atmos_adj == 1, atmos_LAGS[x])  # get lags for atmos links
            #lag = None



            distance = grid.angular_distance()[0:nr_land_nodes,
                       nr_land_nodes:nr_nodes_without_ocean] * EARTH_RADIUS  # get angular distances between the networks
            distance = np.extract(adj == 1, distance)
            land_distance = grid.angular_distance()[0:nr_land_nodes,
                            0:nr_land_nodes] * EARTH_RADIUS  # get angular distances between the networks
            land_distance = np.extract(land_adj == 1, land_distance)
            atmos_distance = grid.angular_distance()[nr_land_nodes:nr_nodes_without_ocean,
                             nr_land_nodes:nr_nodes_without_ocean] * EARTH_RADIUS  # get angular distances between the networks
            atmos_distance = np.extract(atmos_adj == 1, atmos_distance)

            lags_distance = np.divide(lags.astype('float32'), distance, out=lags.astype('float32'), where=distance != 0)
            land_lags_distance = np.divide(land_lags.astype('float32'), land_distance, out=land_lags.astype('float32'),
                                           where=land_distance != 0)
            atmos_lags_distance = np.divide(atmos_lags.astype('float32'), atmos_distance,
                                            out=atmos_lags.astype('float32'),
                                            where=atmos_distance != 0)

            #
            #  Save results to text file
            #
            print("saving results...")

            # save variables in filename

            lags_name = dir_directory + "/" + 'lags_' + info_name + '.txt'
            land_lags_name = land_directory + "/" + 'lags_' + info_name + '.txt'
            atmos_lags_name = atmos_directory + "/" + 'lags_' + info_name + '.txt'

            distance_name = dir_directory + "/" + 'distance_' + info_name + '.txt'
            land_distance_name = land_directory + "/" + 'distance_' + info_name + '.txt'
            atmos_distance_name = atmos_directory + "/" + 'distance_' + info_name + '.txt'

            lags_distance_name = dir_directory + "/" + 'lags_distance_' + info_name + '.txt'
            land_lags_distance_name = land_directory + "/" + 'lags_distance_' + info_name + '.txt'
            atmos_lags_distance_name = atmos_directory + "/" + 'lags_distance_' + info_name + '.txt'

            # save info name
            np.savetxt(dir_directory + "/" + info_name, [0])

            # save lags
            np.savetxt(lags_name, lags)
            np.savetxt(land_lags_name, land_lags)
            np.savetxt(atmos_lags_name, atmos_lags)
            # np.savetxt(lags_distance_name, lags_distance)

            # save distance
            np.savetxt(distance_name, distance)
            np.savetxt(land_distance_name, land_distance)
            np.savetxt(atmos_distance_name, atmos_distance)

            np.savetxt(lags_distance_name, lags_distance)
            np.savetxt(land_lags_distance_name, land_lags_distance)
            np.savetxt(atmos_lags_distance_name, atmos_lags_distance)

            if plots == True:
                print('plotting results...')

                label = ['degree', 'clustering']
                OBSERVABLE_NAME = [OBSERVABLE_NAME_1[x], OBSERVABLE_NAME_2[x]]

                if plot_network == 1:
                    X = np.zeros((n_1, n_2), dtype='uint8')
                    X[land_node_list, :] = adj[0:nr_land_nodes, nr_land_nodes:nr_nodes_without_ocean]
                    if x == 1:
                        X = np.transpose(X)
                    X1 = np.zeros((max(X.shape), max(X.shape)), dtype='uint8')
                    X1[0:X.shape[0], 0:X.shape[1]] = X

                    print('plotting network...')
                    plot_network(X1, x, 'Cross Layer Network', lags, 2, lon, lat, lons, lats, Nodes)
                    plt.savefig(dir_directory + "/" + 'cross_l_network_' + info_name + '.pdf')
                    plt.close()
                    X1 = None
                    X = None

                #
                # plot correlation, distance and max lag histograms
                #

                plt.clf()
                plt.hist(lags, bins=100)
                plt.xlabel("lag_" + OBSERVABLE_NAME_1[x] + '_' + OBSERVABLE_NAME_2[x])
                plt.savefig(
                    dir_directory + "/" + "lag_" + OBSERVABLE_NAME_1[x] + '_' + OBSERVABLE_NAME_2[
                        x] + info_name + ".pdf")
                plt.clf()
                plt.hist(land_lags, bins=100)
                plt.xlabel("lag_" + OBSERVABLE_NAME1)
                plt.savefig(
                    land_directory + "/" + "lag_" + info_name + ".pdf")

                plt.clf()
                plt.hist(atmos_lags, bins=100)
                plt.xlabel("lag_" + OBSERVABLE_NAME2)
                plt.savefig(
                    atmos_directory + "/" + "lag_" + info_name + ".pdf")

                plt.clf()
                plt.hist(distance, bins=100)
                plt.xlabel("geodesic distance in km " + OBSERVABLE_NAME_1[x] + '_' + OBSERVABLE_NAME_2[x])
                plt.savefig(
                    dir_directory + "/" + "distance_" + OBSERVABLE_NAME_1[x] + '_' + OBSERVABLE_NAME_2[
                        x] + info_name + ".pdf")
                plt.clf()
                plt.clf()
                plt.hist(land_distance, bins=100)
                plt.xlabel("geodesic distance in km " + OBSERVABLE_NAME1)
                plt.savefig(
                    land_directory + "/" + "distance_" + info_name + ".pdf")

                plt.clf()
                plt.hist(atmos_distance, bins=100)
                plt.xlabel("geodesic distance in km " + OBSERVABLE_NAME2)
                plt.savefig(
                    atmos_directory + "/" + "distance_" + info_name + ".pdf")

                plt.clf()
                plt.hist(lags_distance, bins=100)
                plt.xlabel(
                    "lags in months / geodesic distance in km " + OBSERVABLE_NAME_1[x] + '_' + OBSERVABLE_NAME_2[x])
                plt.savefig(
                    dir_directory + "/" + "lags_distance_" + OBSERVABLE_NAME_1[x] + '_' + OBSERVABLE_NAME_2[
                        x] + info_name + ".pdf")
                plt.clf()
                plt.clf()
                plt.hist(land_lags_distance, bins=100)
                plt.xlabel("lags in months / geodesic distance in km " + OBSERVABLE_NAME1)
                plt.savefig(
                    land_directory + "/" + "lags_distance_" + info_name + ".pdf")

                plt.clf()
                plt.hist(atmos_lags_distance, bins=100)
                plt.xlabel("lags in months / geodesic distance in km " + OBSERVABLE_NAME2)
                plt.savefig(
                    atmos_directory + "/" + "lags_distance_" + info_name + ".pdf")

            sern_folder = os.path.join(dir_directory, 'sern')
            if not os.path.exists(sern_folder):
                os.makedirs(sern_folder)
            degree12_dir = os.path.join(sern_folder, 'degree12')
            if not os.path.exists(degree12_dir):
                os.makedirs(degree12_dir)
            degree21_dir = os.path.join(sern_folder, 'degree21')
            if not os.path.exists(degree21_dir):
                os.makedirs(degree21_dir)
            cc12_dir = os.path.join(sern_folder, 'cc12')
            if not os.path.exists(cc12_dir):
                os.makedirs(cc12_dir)
            cc21_dir = os.path.join(sern_folder, 'cc21')
            if not os.path.exists(cc21_dir):
                os.makedirs(cc21_dir)
            land_cc_dir = os.path.join(land_directory, 'cc')
            if not os.path.exists(land_cc_dir):
                os.makedirs(land_cc_dir)
            atmos_cc_dir = os.path.join(atmos_directory, 'cc')
            if not os.path.exists(atmos_cc_dir):
                os.makedirs(atmos_cc_dir)
            land_degree_dir = os.path.join(land_directory, 'degree')
            if not os.path.exists(land_degree_dir):
                os.makedirs(land_degree_dir)
            atmos_degree_dir = os.path.join(atmos_directory, 'degree')
            if not os.path.exists(atmos_degree_dir):
                os.makedirs(atmos_degree_dir)

            hamming_dist_dir = os.path.join(dir_directory, 'hamming_distance')
            if not os.path.exists(hamming_dist_dir):
                os.makedirs(hamming_dist_dir)
            cross_hamming_dist_dir = os.path.join(hamming_dist_dir, 'cross_hamming_distance')
            if not os.path.exists(cross_hamming_dist_dir):
                os.makedirs(cross_hamming_dist_dir)
            land_hamming_dist_dir = os.path.join(hamming_dist_dir, 'land_hamming_distance')
            if not os.path.exists(land_hamming_dist_dir):
                os.makedirs(land_hamming_dist_dir)
            atmos_hamming_dist_dir = os.path.join(hamming_dist_dir, 'atmos_hamming_distance')
            if not os.path.exists(atmos_hamming_dist_dir):
                os.makedirs(atmos_hamming_dist_dir)

            rand_corr_pos = None
            correlation1 = None
            correlation2 = None
            CORR1 = None
            CORR2 = None

            A = None
            B = None
            lags = None
            lag1 = None
            lag2 = None
            lags_distance = None
            LAG = None
            #
            # Initiate network measure arrays
            #

            # degree

            degree12_raw = np.empty((n_1))
            degree12_raw.fill(np.nan)
            land_degree_raw = np.empty((n_1))
            land_degree_raw.fill(np.nan)

            degree21_raw = np.empty((n_2))
            degree21_raw.fill(np.nan)
            atmos_degree_raw = np.empty((n_2))
            atmos_degree_raw.fill(np.nan)

            #betweenness

            betweenness12_raw = np.empty((n_1))
            betweenness12_raw.fill(np.nan)
            land_betweenness_raw = np.empty((n_1))
            land_betweenness_raw.fill(np.nan)

            betweenness21_raw = np.empty((n_2))
            betweenness21_raw.fill(np.nan)
            atmos_betweenness_raw = np.empty((n_2))
            atmos_betweenness_raw.fill(np.nan)

            # local clustering coefficient

            cc12_raw = np.empty((n_1))
            cc12_raw.fill(np.nan)
            land_cc_raw = np.empty((n_1))
            land_cc_raw.fill(np.nan)

            cc21_raw = np.empty((n_2))
            cc21_raw.fill(np.nan)
            atmos_cc_raw = np.empty((n_2))
            atmos_cc_raw.fill(np.nan)

            ADJ = np.empty((nr_nodes_without_ocean, nr_nodes_without_ocean))
            ADJ[0:nr_land_nodes, 0:nr_land_nodes] = land_adj
            ADJ[nr_land_nodes:nr_nodes_without_ocean, nr_land_nodes:nr_nodes_without_ocean] = atmos_adj
            ADJ[0:nr_land_nodes, nr_land_nodes:nr_nodes_without_ocean] = adj
            ADJ[nr_land_nodes:nr_nodes_without_ocean, 0:nr_land_nodes] = np.transpose(adj)


            # get node_weights
            land_node_weights = cos_lat(reduced_lats_joined[0:nr_land_nodes])
            atmos_node_weights = cos_lat(reduced_lats_joined[nr_land_nodes:nr_nodes_without_ocean])
            node_weights = np.concatenate((land_node_weights, atmos_node_weights))

            if nsi == 1:
                # construct nsi interacting network
                net = core.interacting_networks.InteractingNetworks(ADJ,
                                                                    node_weights=node_weights)
                print("calculating corrected nsi network measures...")
                degree12_raw[land_nodes] = net.nsi_cross_degree(land_node_list, atmos_node_list)
                degree21_raw[atmos_nodes] = net.nsi_cross_degree(atmos_node_list, land_node_list)

                betweenness12_raw[land_nodes] = net.nsi_cross_betweenness(land_node_list, atmos_node_list)
                betweenness21_raw[atmos_nodes] = net.nsi_cross_betweenness(atmos_node_list, land_node_list)

                cc12_raw[land_nodes] = net.nsi_cross_local_clustering(land_node_list, atmos_node_list)
                cc21_raw[atmos_nodes] = net.nsi_cross_local_clustering(atmos_node_list,
                                                          land_node_list)
                land_degree_raw[land_nodes] = net.nsi_cross_degree(land_node_list, land_node_list)
                atmos_degree_raw[atmos_nodes] = net.nsi_cross_degree(atmos_node_list, atmos_node_list)
                land_cc_raw[land_nodes] = net.nsi_cross_local_clustering(land_node_list, land_node_list)
                atmos_cc_raw[atmos_nodes] = net.nsi_cross_local_clustering(atmos_node_list,
                                                              atmos_node_list)

                land_betweenness_raw[land_nodes] = net.nsi_cross_betweenness(land_node_list, land_node_list)
                atmos_betweenness_raw[atmos_nodes] = net.nsi_cross_betweenness(atmos_node_list, atmos_node_list)






            else:
                # construct interacting network
                net = core.interacting_networks.InteractingNetworks(ADJ)
                print("calculating corrected network measures...")
                degree12_raw[land_nodes] = net.cross_degree(land_node_list, atmos_node_list)
                degree21_raw[atmos_nodes] = net.cross_degree(atmos_node_list, land_node_list)
                cc12_raw[land_nodes] = net.cross_local_clustering(land_node_list, atmos_node_list)
                cc21_raw[atmos_nodes] = net.cross_local_clustering(atmos_node_list,
                                                      land_node_list)
                betweenness12_raw[land_nodes] = net.nsi_cross_betweenness(land_node_list, atmos_node_list)[
                                                0:nr_land_nodes]
                betweenness21_raw[atmos_nodes] = net.nsi_cross_betweenness(atmos_node_list, land_node_list)[
                                                 nr_land_nodes:nr_nodes_without_ocean]

                land_betweenness_raw[land_nodes] = net.nsi_cross_betweenness(land_node_list, land_node_list)[
                                                   0:nr_land_nodes]
                atmos_betweenness_raw[atmos_nodes] = net.nsi_cross_betweenness(atmos_node_list, atmos_node_list)[
                                                     nr_land_nodes:nr_nodes_without_ocean]

                land_degree_raw[land_nodes] = net.cross_degree(land_node_list, land_node_list)
                atmos_degree_raw[atmos_nodes] = net.cross_degree(atmos_node_list, atmos_node_list)
                land_cc_raw[land_nodes] = net.cross_local_clustering(land_node_list, land_node_list)
                atmos_cc_raw[atmos_nodes] = net.cross_local_clustering(atmos_node_list,
                                                          atmos_node_list)


            adj = None
            ADJ = None
            #
            #  Save results to text file
            #
            print("saving results...")

            # save variables in filename
            degr12_name = dir_directory + "/" + 'degree' + '_' + OBSERVABLE_NAME1 + '_' + info_name
            degr21_name = dir_directory + "/" + 'degree' + '_' + OBSERVABLE_NAME2 + '_' + info_name
            land_degree_name = land_directory + "/" + 'degree' + '_' + info_name
            atmos_degree_name = atmos_directory + "/" + 'degree' + '_' + info_name

            # save variables in filename
            betw12_name = dir_directory + "/" + 'betweenness' + '_' + OBSERVABLE_NAME1 + '_' + info_name
            betw21_name = dir_directory + "/" + 'betweenness' + '_' + OBSERVABLE_NAME2 + '_' + info_name
            land_betweenness_name = land_directory + "/" + 'betweenness' + '_' + info_name
            atmos_betweenness_name = atmos_directory + "/" + 'betweenness' + '_' + info_name

            cc12_name = dir_directory + "/" + 'cc' + '_' + OBSERVABLE_NAME1 + '_' + info_name
            cc21_name = dir_directory + "/" + 'cc' + '_' + OBSERVABLE_NAME2 + '_' + info_name
            land_cc_name = land_directory + "/" + 'cc' + '_' + info_name
            atmos_cc_name = atmos_directory + "/" + 'cc' + '_' + info_name

            # save info name
            np.savetxt(dir_directory + "/" + info_name, [0])

            # save network measures

            np.savetxt(degr12_name + '_raw.txt', degree12_raw)
            np.savetxt(degr21_name + '_raw.txt', degree21_raw)

            np.savetxt(betw12_name + '_raw.txt', betweenness12_raw)
            np.savetxt(betw21_name + '_raw.txt', betweenness21_raw)

            np.savetxt(cc12_name + '_raw.txt', cc12_raw)
            np.savetxt(cc21_name + '_raw.txt', cc21_raw)

            np.savetxt(land_degree_name + '_raw.txt', land_degree_raw)
            np.savetxt(atmos_degree_name + '_raw.txt', atmos_degree_raw)

            np.savetxt(land_betweenness_name + '_raw.txt', land_betweenness_raw)
            np.savetxt(atmos_betweenness_name + '_raw.txt', atmos_betweenness_raw)

            np.savetxt(land_cc_name + '_raw.txt', land_cc_raw)
            np.savetxt(atmos_cc_name + '_raw.txt', atmos_cc_raw)

            print('plotting results...')

            label = ['degree', 'clustering']
            OBSERVABLE_NAME = [OBSERVABLE_NAME_1[x], OBSERVABLE_NAME_2[x]]

            #
            # plot network measures
            #

            print('plotting network measures...')

            measures12_raw = [degree12_raw, cc12_raw]
            measures21_raw = [degree21_raw, cc21_raw]
            land_measures_raw = [land_degree_raw, land_cc_raw]
            atmos_measures_raw = [atmos_degree_raw, atmos_cc_raw]

            plot_cross_map(measures12_raw, 0, lon, lat, nr_lon, nr_lat, OBSERVABLE_NAME, info_name, label)
            plt.savefig(dir_directory + "/" + info_name + '_' + OBSERVABLE_NAME1 + '_raw.pdf')
            plt.close()
            plot_cross_map(measures21_raw, 1, lon, lat, nr_lon, nr_lat, OBSERVABLE_NAME, info_name, label)
            plt.savefig(dir_directory + "/" + info_name + '_' + OBSERVABLE_NAME2 + '_raw.pdf')
            plt.close()


            plot_map(land_measures_raw, 0, lon, lat, nr_lon, nr_lat, OBSERVABLE_NAME1, label)
            plt.savefig(land_directory + "/" + info_name + '_' + OBSERVABLE_NAME1 + '_raw.pdf')
            plt.close()
            plot_map(atmos_measures_raw, 1, lon, lat, nr_lon, nr_lat, OBSERVABLE_NAME2, label)
            plt.savefig(atmos_directory + "/" + info_name + '_' + OBSERVABLE_NAME2 + '_raw.pdf')
            plt.close()

            degree12 = None
            degree21 = None
            cc12 = None
            cc21 = None
            land_degree = None
            atmos_degree = None
            land_cc = None
            atmos_cc = None
            # LAGS=None
        corr_sign = None
    rand_corr_neg = None
    lag = None
    rand_correlation_max = None
    rand_lags = None
    corr = None
    #
    # plot summary plot
    #
    aa
    import pickle
    import numpy as np
    import matplotlib.pyplot as plt
    from mpl_toolkits.basemap import Basemap
    from mpl_toolkits.axes_grid1 import AxesGrid
    import os


    def plot_map_summary(d, lon, lat, nr_lon, nr_lat, title, cols, rows):
        plt.rcParams['font.size'] = 13
        fig, axes = plt.subplots(len(rows), len(cols), figsize=(15, 10))

        fig.suptitle(title)
        for i, ax in zip(range(int(len(rows) * len(cols))), axes.reshape(-1)):

            d[i] = d[i].reshape(nr_lat[i], nr_lon[i])
            m = Basemap(projection='mill', lat_ts=20, llcrnrlon=lon[i].min(), \
                        urcrnrlon=lon[i].max(), llcrnrlat=lat[i].min(), urcrnrlat=lat[i].max(), \
                        resolution='c', ax=ax)
            # convert the lat/lon values to x/y projections.
            x, y = m(*np.meshgrid(lon[i], lat[i]))

            im = m.pcolormesh(x, y, d[i], shading='nearest', cmap=plt.cm.coolwarm, ax=ax)
            # Add a coastline and axis values.
            m.drawcoastlines()
            m.drawparallels(np.arange(-90., 90., 30.), labels=[1, 0, 0, 0])
            m.drawmeridians(np.arange(-180., 180., 60.), labels=[0, 0, 0, 1])
            # ax.set_title(titles[i])
            fig.colorbar(im, ax=ax)
            # cbar = ax.colorbar(im)
            # cbar.set_label(label)

        for ax, col in zip(axes[0], cols):
            ax.set_title(col)

        for ax, row in zip(axes[:, 0], rows):
            ax.set_ylabel(row, rotation=0, size='large', labelpad=100)


    def plot_hist(d, title, cols, rows):
        plt.rcParams['font.size'] = 13
        nr_rows = int(len(rows))
        nr_cols = int(len(cols))
        fig, axes = plt.subplots(nr_rows, nr_cols, figsize=(18, 10))

        fig.suptitle(title)
        for i, ax in zip(range(int(nr_rows * nr_cols)), axes.reshape(-1)):

            ax.hist(d[i], bins=100)
            # plt.xlabel(OBSERVABLE_NAME_1[x] + '->' + OBSERVABLE_NAME_2[x])
            ax.set_aspect('auto')
        for ax, col in zip(axes[0], cols):
            ax.set_title(col)

        for ax, row in zip(axes[:, 0], rows):
            ax.set_ylabel(row, rotation=0, size='large', labelpad=100)


    with open(final_directory + '/OBSERVABLE_NAME1.pkl', 'rb') as f:
        OBSERVABLE_NAME1 = pickle.load(f)
    with open(final_directory + '/OBSERVABLE_NAME2.pkl', 'rb') as f:
        OBSERVABLE_NAME2 = pickle.load(f)
    with open(final_directory + '/{}_WINDOW.pkl'.format(OBSERVABLE_NAME1), 'rb') as f:
        land_WINDOW = pickle.load(f)
    with open(final_directory + '/{}_WINDOW.pkl'.format(OBSERVABLE_NAME2), 'rb') as f:
        atmos_WINDOW = pickle.load(f)

    with open(final_directory + '/perc.pkl', 'rb') as f:
        perc = pickle.load(f)
    plot_directory = os.path.join(final_directory, 'plots')
    if not os.path.exists(plot_directory):
        os.makedirs(plot_directory)
    #
    # get grid information
    #
    gridname1 = final_directory + "/" + 'grid1.obj'
    gridname2 = final_directory + "/" + 'grid2.obj'
    file = open(gridname1, 'rb')
    grid1 = pickle.load(file)
    file.close()
    file = open(gridname2, 'rb')
    grid2 = pickle.load(file)
    file.close()
    land_nodes = np.loadtxt(final_directory + '/{}'.format(OBSERVABLE_NAME1)+'_nodes.txt')
    land_nodes = land_nodes.astype('int')
    atmos_nodes=np.loadtxt(final_directory + '/{}'.format(OBSERVABLE_NAME2)+'_nodes.txt')
    atmos_nodes = atmos_nodes.astype('int')
    nodes_without_ocean = np.loadtxt(final_directory + '/nodes_without_ocean.txt')
    nodes_without_ocean = nodes_without_ocean.astype('int')

    #
    # get grid information
    #

    lat_sequence1 = grid1.lat_sequence()
    n_1 = len(lat_sequence1)
    land_lat_sequence = lat_sequence1[land_nodes]

    lon_sequence1 = grid1.lon_sequence()
    land_lon_sequence = lon_sequence1[land_nodes]

    lat_sequence2 = DATA2.grid.lat_sequence()
    atmos_lat_sequence = lat_sequence2[atmos_nodes]

    lon_sequence2 = DATA2.grid.lon_sequence()
    atmos_lon_sequence = lon_sequence2[atmos_nodes]
    lats_joined = np.concatenate((lat_sequence1, lat_sequence2))
    lons_joined = np.concatenate((lon_sequence1, lon_sequence2))
    reduced_lats_joined = np.concatenate((land_lat_sequence, lat_sequence2))
    reduced_lons_joined = np.concatenate((land_lon_sequence, lon_sequence2))

    nr_land_nodes = len(land_nodes)
    nr_nodes_without_ocean = len(nodes_without_ocean)
    nr_atmos_nodes = len(atmos_node_list)

    land_node_list = range(nr_land_nodes)
    atmos_node_list = range(nr_land_nodes, nr_nodes_without_ocean, 1)


    nodelist_without_ocean = range(len(nodes_without_ocean))



    lat1 = np.unique(lat_sequence1, return_counts=True)
    lat2 = np.unique(lat_sequence2, return_counts=True)
    lon1 = np.unique(lon_sequence1, return_counts=True)
    lon2 = np.unique(lon_sequence2, return_counts=True)
    lat_joined = np.unique(lats_joined)
    lon_joined = np.unique(lons_joined)
    reduced_lat_joined = np.unique(reduced_lats_joined)
    reduced_lon_joined = np.unique(reduced_lons_joined)
    nr_lon = [len(lon2[0]), len(lon2[0]), len(lon2[0]), len(lon2[0]), len(lon1[0]), len(lon1[0]), len(lon1[0]),
              len(lon1[0])]
    nr_lat = [len(lat2[0]), len(lat2[0]), len(lat2[0]), len(lat2[0]), len(lat1[0]), len(lat1[0]), len(lat1[0]),
              len(lat1[0])]
    lon = [lon2[0], lon2[0], lon2[0], lon2[0], lon1[0], lon1[0], lon1[0], lon1[0]]
    lat = [lat2[0], lat2[0], lat2[0], lat2[0], lat1[0], lat1[0], lat1[0], lat1[0]]

    signum = ['positive', 'negative']
    sign = [1, -1]
    s = 0
    OBSERVABLE_NAME_1 = [OBSERVABLE_NAME1, OBSERVABLE_NAME2]
    OBSERVABLE_NAME_2 = [OBSERVABLE_NAME2, OBSERVABLE_NAME1]

    degree12 = [0, 0, 0, 0]
    degree21 = [0, 0, 0, 0]
    cc12 = [0, 0, 0, 0]
    cc21 = [0, 0, 0, 0]
    land_degree = [0, 0, 0, 0]
    atmos_degree = [0, 0, 0, 0]
    land_cc = [0, 0, 0, 0]
    atmos_cc = [0, 0, 0, 0]

    lags = [0, 0, 0, 0]
    land_lags = [0, 0, 0, 0]
    atmos_lags = [0, 0, 0, 0]
    distance = [0, 0, 0, 0]
    land_distance = [0, 0, 0, 0]
    atmos_distance = [0, 0, 0, 0]
    lags_distance = [0, 0, 0, 0]
    land_lags_distance = [0, 0, 0, 0]
    atmos_lags_distance = [0, 0, 0, 0]
    threshold_land = [0, 0, 0, 0]
    threshold_atmos = [0, 0, 0, 0]

    count = 0
    for s in [0, 1]:
        print(signum[s])
        signum_directory = final_directory + '/' + signum[s]

        #
        # loop over both directions, land -> atmos and atmos -> land
        #

        for x in range(2):
            dir_directory = signum_directory + '/' + OBSERVABLE_NAME_1[x] + '-' + OBSERVABLE_NAME_2[x]
            land_directory = dir_directory + '/{}'.format(OBSERVABLE_NAME1)
            atmos_directory = dir_directory + '/{}'.format(OBSERVABLE_NAME2)
            sern_folder = dir_directory + '/sern'

            info_name = OBSERVABLE_NAME_1[x] + '-' + OBSERVABLE_NAME_2[x] + '_percentile={}'.format(perc) \
                        + '_' + signum[s]
            # filenames
            degr12_name = dir_directory + "/" + 'degree' + '_' + OBSERVABLE_NAME1 + '_' + info_name + '_raw'
            degr21_name = dir_directory + "/" + 'degree' + '_' + OBSERVABLE_NAME2 + '_' + info_name + '_raw'
            land_degree_name = land_directory + "/" + 'degree' + '_' + info_name + '_raw'
            atmos_degree_name = atmos_directory + "/" + 'degree' + '_' + info_name + '_raw'

            cc12_name = dir_directory + "/" + 'cc' + '_' + OBSERVABLE_NAME1 + '_' + info_name + '_raw'
            cc21_name = dir_directory + "/" + 'cc' + '_' + OBSERVABLE_NAME2 + '_' + info_name + '_raw'
            land_cc_name = land_directory + "/" + 'cc' + '_' + info_name + '_raw'
            atmos_cc_name = atmos_directory + "/" + 'cc' + '_' + info_name + '_raw'

            # load network measures
            degree12[count] = np.loadtxt(degr12_name + '.txt')
            degree21[count] = np.loadtxt(degr21_name + '.txt')
            cc12[count] = np.loadtxt(cc12_name + '.txt')
            cc21[count] = np.loadtxt(cc21_name + '.txt')

            land_degree[count] = np.loadtxt(land_degree_name + '.txt')
            atmos_degree[count] = np.loadtxt(atmos_degree_name + '.txt')
            land_cc[count] = np.loadtxt(land_cc_name + '.txt')
            atmos_cc[count] = np.loadtxt(atmos_cc_name + '.txt')

            lags_name = dir_directory + "/" + 'lags_' + info_name + '.txt'
            land_lags_name = land_directory + "/" + 'lags_' + info_name + '.txt'
            atmos_lags_name = atmos_directory + "/" + 'lags_' + info_name + '.txt'

            distance_name = dir_directory + "/" + 'distance_' + info_name + '.txt'
            land_distance_name = land_directory + "/" + 'distance_' + info_name + '.txt'
            atmos_distance_name = atmos_directory + "/" + 'distance_' + info_name + '.txt'

            lags_distance_name = dir_directory + "/" + 'lags_distance_' + info_name + '.txt'
            land_lags_distance_name = land_directory + "/" + 'lags_distance_' + info_name + '.txt'
            atmos_lags_distance_name = atmos_directory + "/" + 'lags_distance_' + info_name + '.txt'

            # load lags
            lags[count] = np.loadtxt(lags_name)
            land_lags[count] = np.loadtxt(land_lags_name)
            atmos_lags[count] = np.loadtxt(atmos_lags_name)

            # load distance
            distance[count] = np.loadtxt(distance_name)
            land_distance[count] = np.loadtxt(land_distance_name)
            atmos_distance[count] = np.loadtxt(atmos_distance_name)


            threshold_land[count] = np.loadtxt(
                dir_directory + '/threshold_' + OBSERVABLE_NAME1 + '_' + signum[s] + '.txt')
            threshold_atmos[count] = np.loadtxt(
                dir_directory + '/threshold_' + OBSERVABLE_NAME2 + '_' + signum[s] + '.txt')

            count += 1

    OBSERVABLE_NAME_1 = [OBSERVABLE_NAME1, OBSERVABLE_NAME2]
    OBSERVABLE_NAME_2 = [OBSERVABLE_NAME2, OBSERVABLE_NAME1]

    jj = [2, 3]
    ii = [0, 1]
    for x in range(2):
        title = 'Degrees,   ' + OBSERVABLE_NAME_1[x] + ' -> ' + OBSERVABLE_NAME_2[x]
        cols = ['positive correlation', 'negative correlation']
        rows = [OBSERVABLE_NAME2, 'cross-' + OBSERVABLE_NAME2, 'cross-' + OBSERVABLE_NAME1, OBSERVABLE_NAME1]

        degrees = [atmos_degree[ii[x]], atmos_degree[jj[x]], degree21[ii[x]], degree21[jj[x]], degree12[ii[x]],
                   degree12[jj[x]], land_degree[ii[x]],
                   land_degree[jj[x]]]
        plt.clf()
        plot_map_summary(degrees, lon, lat, nr_lon, nr_lat, title, cols, rows)
        plt.savefig(plot_directory + "/" + 'degrees_' + OBSERVABLE_NAME_1[x] + ' -> ' + OBSERVABLE_NAME_2[x] + '.png')
        plt.close()

        title = 'Local Cluster Coefficient,   ' + OBSERVABLE_NAME_1[x] + ' -> ' + OBSERVABLE_NAME_2[x]

        cc = [atmos_cc[ii[x]], atmos_cc[jj[x]], cc21[ii[x]], cc21[jj[x]], cc12[ii[x]], cc12[jj[x]], land_cc[ii[x]],
              land_cc[jj[x]]]
        plt.clf()
        plot_map_summary(cc, lon, lat, nr_lon, nr_lat, title, cols, rows)
        plt.savefig(plot_directory + "/" + 'cc_' + OBSERVABLE_NAME_1[x] + ' -> ' + OBSERVABLE_NAME_2[x] + '.png')
        plt.close()

    nr_lon = [len(lon2[0]), len(lon2[0]), len(lon1[0]),
              len(lon1[0])]
    nr_lat = [len(lat2[0]), len(lat2[0]), len(lat1[0]),
              len(lat1[0])]
    lon = [lon2[0], lon2[0], lon1[0], lon1[0]]
    lat = [lat2[0], lat2[0], lat1[0], lat1[0]]

    title = 'Correlations ' + '{}'.format(perc) + ' - Percentile'
    cols = ['positive correlation', 'negative correlation']
    rows = [OBSERVABLE_NAME2, OBSERVABLE_NAME1]

    #
    # loop over both directions, land -> atmos and atmos -> land
    #
    jj = [2, 3]
    ii = [0, 1]
    ll = [0, 2]
    for x in range(2):
        title = 'Correlations ' + '{}'.format(perc) + ' - Percentile'
        cols = ['positive correlation', 'negative correlation']
        rows = [OBSERVABLE_NAME2, OBSERVABLE_NAME1]
        threshold = [threshold_atmos[ii[x]], threshold_atmos[jj[x]], threshold_land[ii[x]], threshold_land[jj[x]]]
        plot_map_summary(threshold, lon, lat, nr_lon, nr_lat, title, cols, rows)
        plt.savefig(
            plot_directory + "/" + "thresholds_" + OBSERVABLE_NAME_1[x] + ' -> ' + OBSERVABLE_NAME_2[x] + ".png")
        plt.close()

        cols = [OBSERVABLE_NAME1 + ' -> ' + OBSERVABLE_NAME2, OBSERVABLE_NAME2 + ' -> ' + OBSERVABLE_NAME1]
        rows = [OBSERVABLE_NAME2, OBSERVABLE_NAME1]
        threshold = [threshold_atmos[ll[x]], threshold_atmos[ll[x] + 1], threshold_land[ll[x]],
                     threshold_land[ll[x] + 1]]
        plot_map_summary(threshold, lon, lat, nr_lon, nr_lat, title, cols, rows)
        plt.savefig(
            plot_directory + "/" + "thresholds_" + signum[x] + ".png")
        plt.close()

        title = 'Lags in Months'
        cols = ['positive correlation', 'negative correlation']
        rows = [OBSERVABLE_NAME2, 'cross', OBSERVABLE_NAME1]
        Lags = [atmos_lags[ii[x]], atmos_lags[jj[x]], lags[ii[x]], lags[jj[x]], land_lags[ii[x]], land_lags[jj[x]]]
        plt.clf()
        try:
            plot_hist(Lags, title, cols, rows)
            plt.savefig(
                plot_directory + "/" + "lags_" + OBSERVABLE_NAME_1[x] + ' -> ' + OBSERVABLE_NAME_2[x] + ".png")
            plt.close()
        except Exception:
            continue



        cols = [OBSERVABLE_NAME1 + ' -> ' + OBSERVABLE_NAME2, OBSERVABLE_NAME2 + ' -> ' + OBSERVABLE_NAME1]
        Lags = [atmos_lags[ll[x]], atmos_lags[ll[x] + 1], lags[ll[x]], lags[ll[x] + 1], land_lags[ll[x]],
                land_lags[ll[x] + 1]]
        plt.clf()
        plot_hist(Lags, title, cols, rows)
        plt.savefig(
            plot_directory + "/" + "lags_" + signum[x] + ".png")
        plt.close()

        title = 'Geodesic Distance in km'
        cols = ['positive correlation', 'negative correlation']
        rows = [OBSERVABLE_NAME2, 'cross', OBSERVABLE_NAME1]
        distances = [atmos_distance[ii[x]], atmos_distance[jj[x]], distance[ii[x]], distance[jj[x]],
                     land_distance[ii[x]],
                     land_distance[jj[x]]]
        plt.clf()
        plot_hist(distances, title, cols, rows)
        plt.savefig(
            plot_directory + "/" + "distance_" + OBSERVABLE_NAME_1[x] + ' -> ' + OBSERVABLE_NAME_2[x] + ".png")
        plt.close()

        title = 'Geodesic Distance in km'
        cols = [OBSERVABLE_NAME1 + ' -> ' + OBSERVABLE_NAME2, OBSERVABLE_NAME2 + ' -> ' + OBSERVABLE_NAME1]
        rows = [OBSERVABLE_NAME2, 'cross', OBSERVABLE_NAME1]
        distances = [atmos_distance[ll[x]], atmos_distance[ll[x] + 1], distance[ll[x]], distance[ll[x] + 1],
                     land_distance[ll[x]],
                     land_distance[ll[x] + 1]]
        plt.clf()
        plot_hist(distances, title, cols, rows)
        plt.savefig(
            plot_directory + "/" + "distance_" + signum[x] + ".png")
        plt.close()

