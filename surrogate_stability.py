import copy
from pyunicorn import climate, funcnet, timeseries, core
from pyunicorn.core.geo_grid import GeoGrid
from plot import plot_measure, plot_network
import scipy.stats.mstats as mstats
import os
import shutil
import numpy as np
from pyunicorn import core
import matplotlib.pyplot as plt
from plot import plot_map, plot_cross_map
import pickle
import matplotlib as mpl

quartile1 = 25
quartile2 = 75

# land data filename
DATA_FILENAME1 = "/home/eggo/PIK/data/climate_data/grid3/monthly_MODIS_GPP_2004_2018_regrid3.nc"
# atmos data filename
DATA_FILENAME2 = "/home/eggo/PIK/data/climate_data/grid3/monthly_TRMM_precip_2004_2018_regrid3.nc4"

# DATA_FILENAME1 = "/home/eggo/piktmp/climate_data/for_use/grid05/monthly_GPP_MODIS_2001_2020.nc"
# DATA_FILENAME2 = "/home/eggo/piktmp/climate_data/for_use/grid05/solar_radiation_regrid05_2001_2020.nc"

# land data filename
DATA_FILENAME1 = "/home/eggo/piktmp/climate_data/for_use/grid05/2003_2019/GPP.nc"
# atmos data filename
DATA_FILENAME2 = "/home/eggo/piktmp/climate_data/for_use/grid05/2003_2019/precipitation.nc"

# temporal and spatial window of interest
WINDOW2 = {'time_min': 0.0, 'time_max': 0.0, "lat_min": 3
    , "lon_min": -55
    , "lat_max": 4
    , "lon_max": -45}

WINDOW1 = {'time_min': 0.0, 'time_max': 0.0, "lat_min": 3
    , "lon_min": -55
    , "lat_max": 4
    , "lon_max": -45}

WINDOW1 = {"time_min": 0., "time_max": 0., "lat_min": -2
    , "lon_min": -55
    , "lat_max": 5
    , "lon_max": -45}
WINDOW2 = {"time_min": 0., "time_max": 0., "lat_min": -2
    , "lon_min": -55
    , "lat_max": 5
    , "lon_max": -45}

# observable name for land data
OBSERVABLE_NAME1 = "GPP"
# observable name for atmos data
OBSERVABLE_NAME2 = "precipitation"
# OBSERVABLE_NAME2 = "ssrd"
time_range = '2001_2020'  # str(sys.argv[6])
job_nr = 4  # number of parallel executions, must be factor of M and compare_nr
nr_surrogates = 10  # number of surrogate time series for each cell
M = 24  # number of surrogate networks (sern)
tau = 3  # number of lags
nsi = 1
compare_nr = 8  # must be factor of M
plots = True

fontsize = 16
mylinewidth = 3

pgf_with_latex = {  # setup matplotlib to use latex for output
    "pgf.texsystem": "pdflatex",  # change this if using xetex or lautex
    "text.usetex": True,  # use LaTeX to write all text
    "font.family": "serif",

    # "font.family": "sans-serif",
    # "font.sans-serif": ["Computer Modern"],
    "font.serif": [],  # blank entries should cause plots
    "font.sans-serif": [],  # to inherit fonts from the document
    "font.monospace": [],
    "axes.labelsize": fontsize,  # LaTeX default is 10pt font.
    "font.size": fontsize,
    "legend.fontsize": fontsize,  # Make the legend/label fonts
    "xtick.labelsize": fontsize,  # a little smaller
    "ytick.labelsize": fontsize,
    "axes.linewidth": mylinewidth,
    "figure.figsize": (11, 7),  # default fig size of 0.9 textwidth
    "pgf.preamble": "\n".join([  # plots will use this preamble
        r"\usepackage[utf8]{inputenc}",
        r"\usepackage[T1]{fontenc}",
        # r"\usepackage[detect-all,locale=DE]{siunitx}",
    ])
}
mpl.rcParams.update(pgf_with_latex)

noise = 0  # if true noise is added to deseasonalized data
noise_factor = 0.1  # factor to scale mean of deseasonalized data. scaled mean is the noise added

save_sern_adj = 0  # if 1, sern adjacency matrices are savedsave_sern_adj=0 # if 1, sern adjacency matrices are saved

compare_factor = int(M / compare_nr)
if M / job_nr - int(M / job_nr) != 0:
    raise ValueError("job nr must be factor of M")

if compare_factor - M / compare_nr != 0:
    raise ValueError("job nr must be factor of M")

#  Path and filename of NetCDF file containing climate data

print("time range= {}".format(time_range))

EARTH_RADIUS = 6367.5

#  Type of data file ("NetCDF" indicates a NetCDF file with data on a regular
#  lat-lon grid, "iNetCDF" allows for arbitrary grids - > see documentation).
FILE_TYPE = "NetCDF"
#  Indicate the length of the annual cycle in the data (e.g., 12 for monthly
#  data)
TIME_CYCLE = 12

#  Indicates whether to use only data from winter months (DJF) for calculating
#  correlations
WINTER_ONLY = False


def cos_lat(lat_sequence):
    return np.cos(lat_sequence * np.pi / 180)


#
# create filename
#


filename = 'surrogate_stability_' + OBSERVABLE_NAME1 + '_' + OBSERVABLE_NAME2 + '_' + 'lat_min={}'.format(
    WINDOW1['lat_min']) + '_{}'.format(WINDOW2['lat_min']) + '_' + 'lon_min={}'.format(
    WINDOW1['lon_min']) + '_{}'.format(WINDOW2['lon_min']) + '_' \
           + 'lat_max={}'.format(WINDOW1['lat_max']) + '_{}'.format(WINDOW2['lat_max']) + '_' + 'lon_max={}'.format(
    WINDOW1['lon_max']) \
           + '_{}'.format(WINDOW2['lon_max']) + '_tau_max={}'.format(
    tau) + '_' + time_range

#
# create results folder
#

current_directory = os.getcwd()
final_directory = os.path.join(current_directory, filename)
if not os.path.exists(final_directory):
    os.makedirs(final_directory)
with open('final_directory.txt', "w") as text_file:
    text_file.write(final_directory)
with open('final_directory.pkl', 'wb') as f:
    pickle.dump(final_directory, f)
with open(final_directory + '/land_WINDOW.pkl', 'wb') as f:
    pickle.dump(WINDOW1, f)
with open(final_directory + '/atmos_WINDOW.pkl', 'wb') as f:
    pickle.dump(WINDOW2, f)

with open(final_directory + '/OBSERVABLE_NAME1.pkl', 'wb') as f:
    pickle.dump(OBSERVABLE_NAME1, f)
with open(final_directory + '/OBSERVABLE_NAME2.pkl', 'wb') as f:
    pickle.dump(OBSERVABLE_NAME2, f)

with open(final_directory + '/OBSERVABLE_NAME1.txt', "w") as text_file:
    text_file.write(OBSERVABLE_NAME1)
with open(final_directory + '/OBSERVABLE_NAME2.txt', "w") as text_file:
    text_file.write(OBSERVABLE_NAME2)

with open(final_directory + '/job_nr.pkl', 'wb') as f:
    pickle.dump(job_nr, f)
with open(final_directory + '/nr_surrogates.pkl', 'wb') as f:
    pickle.dump(nr_surrogates, f)
with open(final_directory + '/tau.pkl', 'wb') as f:
    pickle.dump(tau, f)

with open(final_directory + '/M.pkl', 'wb') as f:
    pickle.dump(M, f)

with open(final_directory + '/nsi.pkl', 'wb') as f:
    pickle.dump(nsi, f)
with open(final_directory + '/compare_nr.pkl', 'wb') as f:
    pickle.dump(compare_nr, f)
with open(final_directory + '/save_sern_adj.pkl', 'wb') as f:
    pickle.dump(save_sern_adj, f)
with open(final_directory + '/time_range.pkl', 'wb') as f:
    pickle.dump(time_range, f)

#
# copy main program to results folder, so results can be reproduced
#
shutil.copy(current_directory + '/surrogate_stability.py', final_directory)

#
#  Create a ClimateData object containing the data and print information
#
# initiate Climate Data Class with land data
DATA1 = climate.ClimateData.Load(
    file_name=DATA_FILENAME1, observable_name=OBSERVABLE_NAME1, file_type=FILE_TYPE,
    window=WINDOW1, time_cycle=TIME_CYCLE)

# get mask from land data
mask = np.ma.getmask(DATA1.observable())
# get deseasonalized data and set masked values to nan
anomaly1 = np.where(mask == True, np.nan, DATA1.anomaly())
# get land nodes
land_nodes = np.ndarray.flatten(np.argwhere(~np.isnan(anomaly1[0, :])))
np.savetxt(final_directory + '/{}'.format(OBSERVABLE_NAME1) + '_nodes.txt', land_nodes, fmt='%1.1i')

# get ocean nodes
ocean_nodes1 = np.ndarray.flatten(np.argwhere(np.isnan(anomaly1[0, :])))
anomaly1_reduced = np.delete(anomaly1, ocean_nodes1, 1)

# initiate Climate Data Class with atmosphere data
DATA2 = climate.ClimateData.Load(
    file_name=DATA_FILENAME2, observable_name=OBSERVABLE_NAME2, file_type=FILE_TYPE,
    window=WINDOW2, time_cycle=TIME_CYCLE)

# get deseasonalized atmos data
anomaly2 = DATA2.anomaly()
# get mask from land data
mask = np.ma.getmask(DATA2.observable())
# get deseasonalized data and set masked values to nan
anomaly2 = np.where(mask == True, np.nan, DATA2.anomaly())
atmos_nodes = np.ndarray.flatten(np.argwhere(~np.isnan(anomaly2[0, :])))
np.savetxt(final_directory + '/{}'.format(OBSERVABLE_NAME2) + '_nodes.txt', atmos_nodes, fmt='%1.1i')
ocean_nodes = np.ndarray.flatten(np.argwhere(np.isnan(anomaly2[0, :])))
anomaly2_reduced = np.delete(anomaly2, ocean_nodes, 1)
joined_anomaly = np.concatenate((anomaly1[0, :], anomaly2[0, :]), axis=0)

nodes_without_ocean = np.ndarray.flatten(np.argwhere(~np.isnan(joined_anomaly)))
np.savetxt(final_directory + '/nodes_without_ocean.txt', nodes_without_ocean, fmt='%1.1i')

#
# add noise to deseasonalized data
#


# rank anomaly values for rank correlation
anomaly1_ranked = mstats.rankdata(anomaly1_reduced, axis=0)  # anomaly1_reduced.argsort(axis=0).argsort(axis=0) + 1.0#
anomaly1_ranked_b = mstats.rankdata(anomaly1_reduced)  # anomaly1_reduced.argsort(axis=0).argsort(axis=0) + 1.0#
plt.plot(anomaly1_ranked[:,0])
plt.savefig(final_directory+'/ranked.png')
plt.close()
plt.plot(anomaly1_ranked_b[:,0])
plt.savefig(final_directory+'/ranked_flattened.png')
aa
anomaly2_ranked = mstats.rankdata(anomaly2_reduced, axis=0)  # anomaly2.argsort(axis=0).argsort(axis=0) + 1.0#


# anomaly = np.concatenate((anomaly1_reduced, anomaly2), axis=1) # pearson
anomaly = np.concatenate((anomaly1_ranked, anomaly2_ranked),
                         axis=1)  # take ranked values for spearman rank correlation
#
# get grid information
#

lat_sequence1 = DATA1.grid.lat_sequence()
land_lat_sequence = lat_sequence1[land_nodes]

lon_sequence1 = DATA1.grid.lon_sequence()
land_lon_sequence = lon_sequence1[land_nodes]

lat_sequence2 = DATA2.grid.lat_sequence()
lon_sequence2 = DATA2.grid.lon_sequence()

lats_joined = np.concatenate((lat_sequence1, lat_sequence2))
reduced_lats_joined = np.concatenate((land_lat_sequence, lat_sequence2))
lons_joined = np.concatenate((lon_sequence1, lon_sequence2))

reduced_lons_joined = np.concatenate((land_lon_sequence, lon_sequence2))
lons = [lon_sequence1, lon_sequence2, lons_joined]
lats = [lat_sequence1, lat_sequence2, lats_joined]
# get number of nodes in network 1
n_1 = len(lons[0])
# get number of nodes in network 2
n_2 = len(lons[1])
N = len(lons[2])
# get total number of nodes in joined network
Nodes = range(N)
land_node_list = range(len(land_nodes))
atmos_node_list = range(len(land_nodes), len(nodes_without_ocean), 1)
nodelist_without_ocean = range(len(nodes_without_ocean))
nr_land_nodes = len(land_nodes)
nr_nodes_without_ocean = len(nodes_without_ocean)
lat1 = np.unique(lat_sequence1, return_counts=True)
lat2 = np.unique(lat_sequence2, return_counts=True)
lon1 = np.unique(lon_sequence1, return_counts=True)
lon2 = np.unique(lon_sequence2, return_counts=True)
lat_joined = np.unique(lats_joined)
lon_joined = np.unique(lons_joined)

reduced_lat_joined = np.unique(reduced_lats_joined)
reduced_lon_joined = np.unique(reduced_lons_joined)
nr_lat = [len(lat1[0]), len(lat2[0]), len(lat_joined)]
nr_lon = [len(lon1[0]), len(lon2[0]), len(lon_joined)]
lon = [lon1[0], lon2[0], lon_joined]
lat = [lat1[0], lat2[0], lat_joined]

nr_time = anomaly1_ranked.shape[0]

#
# construct coupling analysis instance for all data, land data and atmos data
#
print('calculating correlations ...')
FUNCNET = funcnet.CouplingAnalysis(anomaly)

#
# get max correlation and corresponding lag
#

# correlation = funcnet.cross_correlation(tau_max=tau, lag_mode='all')

# get maximal correlation and corresponding lags
corr = FUNCNET.cross_correlation(tau_max=tau, lag_mode='max')[0]

# np.savetxt(final_directory + '/atmos_lags_matrix.txt', atmos_LAGS[0])
# np.savetxt(final_directory + '/land_lags_matrix.txt', land_LAGS[0])

'''
#
# get surrogates
#

# initiate Surrogates instance with land data
surrogates1 = np.empty((anomaly1_ranked.shape[0], anomaly1_ranked.shape[1], nr_surrogates))

rand_correlation_max_land_1 = np.empty((nr_land_nodes, nr_surrogates))

# get correlated noise surrogates for land data
for i in range(nr_surrogates):
    surrogate = timeseries.surrogates.Surrogates(np.transpose(anomaly1_ranked), silence_level=2)
    surrogates1[:, :, i] = np.transpose(surrogate.correlated_noise_surrogates(np.transpose(anomaly1_ranked)))
# calculate correlation to surrogates
for i in range(nr_land_nodes):
    funcnet1 = funcnet.CouplingAnalysis(anomaly1_ranked[:, i], silence_level=2)
    rand_correlation_max_land_1[i] = funcnet1.pairwise_cross_correlation(surrogates1[:, i, :], tau_max=tau)[0]


surrogates2 = np.empty((anomaly2_ranked.shape[0], anomaly2_ranked.shape[1], nr_surrogates))

rand_correlation_max_atmos_1 = np.empty((n_2, nr_surrogates))
# get correlated noise surrogates for land data
for i in range(nr_surrogates):
    surrogate = timeseries.surrogates.Surrogates(np.transpose(anomaly2_ranked), silence_level=2)
    surrogates2[:, :, i] = np.transpose(surrogate.correlated_noise_surrogates(np.transpose(anomaly2_ranked)))

for i in range(n_2):
    funcnet2 = funcnet.CouplingAnalysis(anomaly2_ranked[:, i], silence_level=2)
    rand_correlation_max_atmos_1[i] = funcnet2.pairwise_cross_correlation(surrogates2[:, i, :], tau_max=tau)[0]

anomaly = None

# concatenate both into one matrix
rand_correlation_max_1 = np.concatenate((rand_correlation_max_land_1, rand_correlation_max_atmos_1), axis=0)
'''


#
# get surrogates
#


# initiate Surrogates instance with land data
surrogates1 = np.empty((anomaly1_ranked.shape[0], anomaly1_ranked.shape[1], nr_surrogates),dtype='float16')

# get correlated noise surrogates for land data
for i in range(nr_surrogates):
    surrogate = timeseries.surrogates.Surrogates(np.transpose(anomaly1_ranked), silence_level=2)
    surrogates1[:, :, i] = np.transpose(surrogate.correlated_noise_surrogates(np.transpose(anomaly1_ranked)))
# calculate correlation to surrogates

rand_correlation_max_land_1 = np.empty((nr_land_nodes, nr_surrogates),dtype='float16')
for i in range(nr_land_nodes):
    funcnet1 = funcnet.CouplingAnalysis(anomaly1_ranked[:, i], silence_level=2)
    rand_correlation_max_land_1[i] = funcnet1.pairwise_cross_correlation(surrogates1[:, i, :], tau_max=tau)[0]
surrogates1=None

surrogates2 = np.empty((anomaly2_ranked.shape[0], anomaly2_ranked.shape[1], nr_surrogates),dtype='float16')
for i in range(nr_surrogates):
    surrogate = timeseries.surrogates.Surrogates(np.transpose(anomaly2_ranked), silence_level=2)
    surrogates2[:, :, i] = np.transpose(surrogate.correlated_noise_surrogates(np.transpose(anomaly2_ranked)))

rand_correlation_max_atmos_1 = np.empty((n_2, nr_surrogates),dtype='float16')
# get correlated noise surrogates for land data

for i in range(n_2):
    funcnet2 = funcnet.CouplingAnalysis(anomaly2_ranked[:, i], silence_level=2)
    rand_correlation_max_atmos_1[i] = funcnet2.pairwise_cross_correlation(surrogates2[:, i, :], tau_max=tau)[0]
surrogates2=None

anomaly = None

# concatenate both into one matrix
rand_correlation_max_1 = np.concatenate((rand_correlation_max_land_1, rand_correlation_max_atmos_1), axis=0,dtype='float16')
rand_correlation_max_land_1 = None
rand_correlation_max_atmos_1 = None
rand_correlation_max_land_2 = None
rand_correlation_max_atmos_2 = None


def single_varianz(measures, nr_sern):
    var = np.empty(nr_sern)

    for i in range(nr_sern):
        measure = measures[0:i + 1]
        var[i] = np.var(measure, axis=0)
    return var


def varianz_per_mean(measures, nr_sern, quantile1, quantile2):
    vars = np.empty((nr_sern, measures.shape[1]))
    means = np.empty((nr_sern, measures.shape[1]))
    var_per_means = np.empty((nr_sern, measures.shape[1]))
    mean = np.empty(nr_sern)
    quant1 = np.empty(nr_sern)
    quant2 = np.empty(nr_sern)

    for i in range(nr_sern):
        measure = measures[0:i + 1, :]
        vars[i] = np.var(measure, axis=0)
        means[i] = np.mean(measure, axis=0)
        var_per_means[i] = vars[i] / means[i]
        mean[i] = np.mean(var_per_means[i], axis=0)
        quant1[i] = np.percentile(var_per_means[i], quantile1)
        quant2[i] = np.percentile(var_per_means[i], quantile2)
    return mean, quant1, quant2


def varianz_per_median(measures, nr_sern, quantile1, quantile2, perc):
    vars = np.empty((nr_sern, measures.shape[1]))
    medians = np.empty((nr_sern, measures.shape[1]))
    var_per_medians = np.empty((nr_sern, measures.shape[1]))
    median = np.empty(nr_sern)
    quant1 = np.empty(nr_sern)
    quant2 = np.empty(nr_sern)

    for i in range(nr_sern):
        measure = measures[0:i + 1, :]
        percentile = np.nanpercentile(measure, perc, axis=0)
        vars[i] = np.var(percentile, axis=0)
        medians[i] = np.median(percentile, axis=0)
        var_per_medians[i] = vars[i] / medians[i]
        median[i] = np.median(var_per_medians[i], axis=0)
        quant1[i] = np.percentile(var_per_medians[i], quantile1)
        quant2[i] = np.percentile(var_per_medians[i], quantile2)
    return median, quant1, quant2


def interquantile_range(measures, nr_sern, quantile1, quantile2, perc):
    quants1 = np.empty((nr_sern, measures.shape[1]))
    quants2 = np.empty((nr_sern, measures.shape[1]))
    interquants = np.empty((nr_sern, measures.shape[1]))
    range_per_percentiles = np.empty((nr_sern, measures.shape[1]))
    median = np.empty(nr_sern)
    quant1 = np.empty(nr_sern)
    quant2 = np.empty(nr_sern)
    percentiles = np.empty((nr_sern, measures.shape[1]))

    for i in range(nr_sern):
        measure = measures[0:i + 1, :]
        percentiles[i, :] = np.nanpercentile(measure, perc, axis=0)
        quants1[i] = np.nanpercentile(percentiles[0:i + 1, :], quantile1, axis=0)
        quants2[i] = np.nanpercentile(percentiles[0:i + 1, :], quantile2, axis=0)
        interquants[i] = quants2[i] - quants1[i]
        range_per_percentiles[i] = interquants[i] / percentiles[i]
        median[i] = np.median(range_per_percentiles[i])
        quant1[i] = np.percentile(range_per_percentiles[i], quantile1)
        quant2[i] = np.percentile(range_per_percentiles[i], quantile2)

    return median, quant1, quant2


def single_interquantile_range(measures, nr_sern, quantile1, quantile2, perc):
    quants1 = np.empty(nr_sern)
    quants2 = np.empty(nr_sern)
    interquants = np.empty(nr_sern)
    range_per_medians = np.empty(nr_sern)
    medians = np.empty(nr_sern)
    percentile = np.empty(nr_sern)

    for i in range(nr_sern):
        measure = measures[0:i + 1]
        percentile[i] = np.nanpercentile(measure, perc, axis=0)
        quants1[i] = np.percentile(percentile, quantile1, axis=0)
        quants2[i] = np.percentile(percentile, quantile2, axis=0)
        interquants[i] = quants2[i] - quants1[i]
        medians[i] = np.median(percentile, axis=0)
        range_per_medians[i] = interquants[i] / medians[i]

    return range_per_medians


perc_10 = 10
perc_50 = 50
perc_90 = 90
perc_99 = 99
percs = [perc_50, perc_90, perc_99]
fig, axs = plt.subplots(len(percs), 1, figsize=(8, 6 * len(percs)))
for i, perc in enumerate(percs):
    var_name = [name for name, val in globals().items() if val is perc][0]

    median, quant1, quant2 = interquantile_range(np.transpose(np.abs(rand_correlation_max_1)), nr_surrogates, quartile1,
                                                 quartile2, perc)
    np.savetxt(final_directory + '/median_' + var_name + '.txt', median)
    np.savetxt(final_directory + '/quant1_' + var_name + '.txt', quant1)
    np.savetxt(final_directory + '/quant2_' + var_name + '.txt', quant2)
    axs[i].plot(median, '--', label='Median', color='r')
    axs[i].plot(quant1, '--', label='Quartiles', color='b')
    axs[i].legend()
    axs[i].plot(quant2, '--', label='Quantile 2', color='b')
    axs[i].set_title('Percentile: ' + str(perc))
    axs[i].set_ylabel('i.r. / {}-perc.'.format(perc))

fig.text(0.5, 0.04, 'surrogates', ha='center')
plt.subplots_adjust(hspace=0.5)
plt.savefig(final_directory + '/subplots.png')
plt.show()
plt.close()
